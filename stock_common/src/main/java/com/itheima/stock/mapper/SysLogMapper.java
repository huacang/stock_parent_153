package com.itheima.stock.mapper;

import com.itheima.stock.pojo.entity.SysLog;

/**
 * @author 46035
 * @description 针对表【sys_log(系统日志)】的数据库操作Mapper
 * @createDate 2023-01-08 15:14:39
 * @Entity com.itheima.stock.pojo.entity.SysLog
 */
public interface SysLogMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysLog record);

    int insertSelective(SysLog record);

    SysLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysLog record);

    int updateByPrimaryKey(SysLog record);

}
