package com.itheima.stock;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
@SpringBootTest
public class TestPasswordEncoder {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void test01(){
        String pwd="1234";
        //加密  $2a$10$WAWV.QEykot8sHQi6FqqDOAnevkluOZJqZJ5YPxSnVVWqvuhx88Ha
        String encode = passwordEncoder.encode(pwd);
        System.out.println(encode);
        /*
            matches()匹配明文密码和加密后密码是否匹配，如果匹配，返回true，否则false
            just test
         */
//        boolean flag = passwordEncoder.matches(pwd, 				          "$2a$10$WAWV.QEykot8sHQi6FqqDOAnevkluOZJqZJ5YPxSnVVWqvuhx88Ha");
//        System.out.println(flag);
    }
    @Test
    public void test02(){
        String pwd="1234";
        String enPwd="$2a$10$DwuqajNiuUfeSsUBVnUnhOfqWPmBjVST7SQqD2Nx/bvW99EWCSGpS";
        boolean isSvcess = passwordEncoder.matches(pwd, enPwd);
        System.out.println(isSvcess?"密码匹配成功":"失败");

    }


}
